# Node-Workshop-Rasa-Chatbot

## Getting started

Information

Python3 version - 3.8.10
pip version - 23.0.1


## Installation

Note: it is adviseable that you create and active a virtualenv before any installation (https://docs.python.org/3/library/venv.html)

`pip install -r requirements.txt`

## RASA commands 

All the commands, and their explanations, can be found here https://rasa.com/docs/rasa/command-line-interface/