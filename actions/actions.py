import os.path

import gspread
import pathlib
import numpy as np


from datetime import datetime, date
from rasa_sdk.types import DomainDict
from typing import Any, Text, Dict, List
from rasa_sdk.events import SlotSet, AllSlotsReset
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action, Tracker, FormValidationAction

meeting_time_map = {
  "NODE": {
    "daily": "Every day at 8:45am",
    "weekly": "Every Thursday at 12:30pm"
  },
  "JAVA": {
    "daily": "Every day at 9am",
    "weekly": "Every Monday at 12:00pm"
  },
  "RUBY": {
    "daily": "Every day at 8:00am",
    "weekly": "Every Friday at 11:15pm"
  },
  "C#": {
    "daily": "Every day at 9:45am",
    "weekly": "Every Wednesday at 10:00pm"
  }
}

class ActionSendMeetingTimeNotice(Action):

    def name(self) -> Text:
        return "action_send_meeting_time_notice"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        technology = tracker.get_slot("technology")
        meeting_type = tracker.get_slot("meeting_type")

        dispatcher.utter_message(text=str(meeting_time_map[technology][meeting_type]))
        return []

class ValidateMeetingTimeForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_meeting_time_form"   

    async def required_slots(
        self,
        domain_slots: List[Text],
        dispatcher: "CollectingDispatcher",
        tracker: "Tracker",
        domain: "DomainDict",
    ) -> List[Text]:
        if tracker.slots.get("technology") is True:
            domain_slots.remove("technology")

        if tracker.slots.get("meeting_type") is True:
            domain_slots.remove("meeting_type")

        return domain_slots

    @staticmethod
    def meeting_types() -> List[Text]:
        return ["weekly", "daily"]

    @staticmethod
    def supported_technologies() -> List[Text]:
        return ["NODE", "RUBY", "JAVA", "C#"]

    def validate_technology(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:

        supported_tech_list = self.supported_technologies()

        if slot_value in supported_tech_list:
            return {"technology": slot_value}
        else:
            message = "I am sorry, but I have only information about these technologies -> " + ' '.join([str(elem) for elem in supported_tech_list]) 
            dispatcher.utter_message(text=message)
            return {"technology": None}

    def validate_meeting_type(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:

        if slot_value.lower() in self.meeting_types():
            return {"meeting_type": slot_value}
        else:
            dispatcher.utter_message(text="I am sorry, but I only have information about these meetings -> " + + ' '.join([str(elem) for elem in supported_tech_list]))
            return {"meeting_type": None}